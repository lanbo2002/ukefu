CREATE TABLE `uk_pbxhost_orgi_rela` (
    `id` varchar(32) NOT NULL COMMENT '主键ID',
    `pbxhostid` varchar(32) DEFAULT NULL COMMENT '联系人id',
    `orgi` varchar(200) DEFAULT NULL COMMENT '关联的租户id',
    `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
    `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
    `createtime` datetime DEFAULT NULL COMMENT '创建时间',
    `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='语音平台 和 租户 绑定';

ALTER TABLE uk_user ADD tenant tinyint(4) DEFAULT 0 COMMENT '是否是新版多租户中新增的用户（true 是）';

ALTER TABLE uk_tenant ADD agentnum INT(11) DEFAULT 0 COMMENT '文本坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD callcenteragentnum INT(11) DEFAULT 0 COMMENT '呼叫中心坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD models VARCHAR(500) DEFAULT NULL COMMENT '授权模块';

ALTER TABLE uk_tenant ADD datastatus tinyint(4) DEFAULT '0' COMMENT '数据状态（0 未删除| 1 已删除到回收站）';

ALTER TABLE uk_callcenter_pbxhost ADD defaultpbx tinyint(4) DEFAULT '0' COMMENT '是否默认 分配的语音服务器';


ALTER TABLE uk_tenant ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';

ALTER TABLE uk_systemconfig ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';
ALTER TABLE uk_systemconfig ADD enablecloud tinyint(4) DEFAULT 0 COMMENT '是否云平台服务';


ALTER TABLE uk_blacklist ADD listtype varchar(50) DEFAULT NULL COMMENT '名单类型(blacklist、黑名单  redlist、红名单  viplist、vip名单)';
ALTER TABLE uk_blacklist ADD extentions varchar(2000) DEFAULT NULL COMMENT '分机号';
ALTER TABLE uk_blacklist ADD skillname varchar(32) DEFAULT NULL COMMENT '技能组名称';

ALTER TABLE uk_callcenter_extention ADD assigned tinyint(4) DEFAULT 0 COMMENT '是否分配 多租户使用';

ALTER TABLE uk_sysdic ADD tenant tinyint(4) DEFAULT 0 COMMENT '是否区分多租户';

-- ----------------------------
-- 更新 系统模板分类 中 输入参数 和 输出参数 （智能机器人接口中使用，区分租户）
-- ----------------------------
update uk_sysdic set tenant = 1 where id in ('4028811b642af06f01642af9cfa304c6','4028811b642af06f01642af9cfaf04c7');


ALTER TABLE uk_reportmodel ADD cubecontent longtext DEFAULT NULL COMMENT '发布模型数据';

ALTER TABLE uk_tabletask ADD share tinyint(4) DEFAULT 0 COMMENT '是否共享（多租户）';

ALTER TABLE uk_callcenter_event ADD conference tinyint DEFAULT 0 COMMENT '是否会议通话';
ALTER TABLE uk_callcenter_event ADD conferenceinitiator tinyint DEFAULT 0 COMMENT '是否会议发起方';
ALTER TABLE uk_callcenter_event ADD inconferenecetime datetime DEFAULT NULL COMMENT '进入会议时间';
ALTER TABLE uk_callcenter_event ADD conferenceduration int DEFAULT 0 COMMENT '会议时长';
ALTER TABLE uk_callcenter_event ADD conferencenum varchar(50) DEFAULT 0 COMMENT '会议号码';
ALTER TABLE uk_callcenter_event ADD conferenceid varchar(50) DEFAULT NULL COMMENT '会议ID';


ALTER TABLE uk_xiaoe_topic ADD enablecc tinyint DEFAULT 0 COMMENT '启用呼叫中心功能';
ALTER TABLE uk_xiaoe_topic ADD cctype varchar(50) DEFAULT NULL COMMENT '呼叫中心类型';

ALTER TABLE uk_xiaoe_topic ADD cctransnum varchar(50) DEFAULT NULL COMMENT '呼叫中心转接号码';

ALTER TABLE uk_xiaoe_topic ADD ccdelay int(11) DEFAULT 0 COMMENT '呼叫中心挂断延迟';

ALTER TABLE `uk_callcenter_router`
    MODIFY COLUMN `field` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段名称';
ALTER TABLE `uk_callcenter_routeritem`
    MODIFY COLUMN `field` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '匹配参数名称';


ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `level` varchar(50) DEFAULT NULL COMMENT '话术评价等级';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `levelscore` int(11) DEFAULT 0 COMMENT '话术评价分数';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `focustimes` int(11) DEFAULT 0 COMMENT '话术关注点次数';
ALTER TABLE `uk_callcenter_event`
    ADD COLUMN `processid` varchar(50) DEFAULT NULL COMMENT '话术or问卷id';
ALTER TABLE `uk_que_result_answer`
    ADD COLUMN `resultquesid` varchar(50) DEFAULT NULL COMMENT '话术问卷节点结果id';
ALTER TABLE `uk_que_result_point`
    ADD COLUMN `resultquesid` varchar(50) DEFAULT NULL COMMENT '话术问卷节点结果id';

ALTER TABLE `uk_que_result_point`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';


ALTER TABLE `uk_que_result_question`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';

ALTER TABLE `uk_que_result`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';
ALTER TABLE `uk_que_result_answer`
    ADD COLUMN `statuseventid` varchar(50) DEFAULT NULL COMMENT '通话记录id';
