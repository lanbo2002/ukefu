package com.ukefu.webim.util.event;

import com.ukefu.webim.util.impl.AiMRoundsProcesserImpl;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.model.AiUser;

public interface AiMessageAskProcesser {
	public ChatMessage process(ChatMessage message, AiMRoundsProcesserImpl aiMroundsProcesser , AiUser aiUser) ;
}
