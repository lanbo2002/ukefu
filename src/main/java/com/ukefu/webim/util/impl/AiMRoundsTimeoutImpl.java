package com.ukefu.webim.util.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ukefu.webim.service.es.MRoundQuestionRepository;
import com.ukefu.webim.util.event.AiMRoundsProcesser;
import com.ukefu.webim.util.server.message.ChatMessage;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.MRoundQuestion;

@Component
public class AiMRoundsTimeoutImpl extends AiMRoundsProcesser{
	
	@Autowired
	private MRoundQuestionRepository mRoundsQuestionRes ;

	@Override
	public ChatMessage process(AiUser aiUser , ChatMessage chat, String question) {
		ChatMessage retChatMessage = null;
		if(chat!=null && aiUser!=null && !StringUtils.isBlank(aiUser.getBussid())) {
			List<MRoundQuestion> mRoundsQuestionList = mRoundsQuestionRes.findByDataidAndOrgi(aiUser.getDataid() , !StringUtils.isBlank(aiUser.getOrgi()) ? aiUser.getOrgi() : chat.getOrgi()) ;
			for(MRoundQuestion mrq : mRoundsQuestionList) {
				/**
				 * 当前节点的 答案
				 */
				if(mrq.getId().equals(aiUser.getBussid())) {
					retChatMessage = overtime(aiUser , mrq , chat); break ;
				}
			}
		}
		return retChatMessage!=null && !StringUtils.isBlank(retChatMessage.getMessage()) && (chat.isDebug() || retChatMessage.isSkip() == false) ? retChatMessage : null ;
	}
}
