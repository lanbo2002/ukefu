package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventRobot implements Serializable,UserEvent{

	private static final long serialVersionUID = -5514665931637791341L;

	private String id ;

	private Integer levelscore;
	private String level;
	private Integer focustimes;

	private String processid;//话术or问卷id\

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	public Integer getLevelscore() {
		return levelscore;
	}

	public void setLevelscore(Integer levelscore) {
		this.levelscore = levelscore;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Integer getFocustimes() {
		return focustimes;
	}

	public void setFocustimes(Integer focustimes) {
		this.focustimes = focustimes;
	}

	public String getProcessid() {
		return processid;
	}

	public void setProcessid(String processid) {
		this.processid = processid;
	}
}
