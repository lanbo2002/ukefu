package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventServicestatus implements Serializable, UserEvent {

	private static final long serialVersionUID = 4164059970184274883L;

	private String id ;

	private String servicestatus ;	//通话状态
	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getServicestatus() {
		return servicestatus;
	}

	public void setServicestatus(String servicestatus) {
		this.servicestatus = servicestatus;
	}

	@Override
	public String toString() {
		return "StatusEventServicestatus{" +
				"id='" + id + '\'' +
				", servicestatus='" + servicestatus + '\'' +
				'}';
	}
}
