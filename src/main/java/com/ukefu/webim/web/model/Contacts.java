package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import com.ukefu.util.UKTools;
import com.ukefu.util.task.ESData;

@Document(indexName = "uckefu", type = "uk_contacts")
@Entity
@Table(name = "uk_contacts")
@org.hibernate.annotations.Proxy(lazy = false)
public class Contacts extends ESBean implements java.io.Serializable ,ESData{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5781401948807231526L;
	private String id  = UKTools.getUUID();
	private String gender;
	@Field(type=FieldType.String,index = FieldIndex.not_analyzed)
	private String cbirthday;
	private String ctype;//联系人contacts、客户customer
	private String ckind;
	private String clevel;
	private String ccode;
	private String nickname;
	private String sarea;
	private String csource;
	private String language;
	
	private String sourcetype ;		//数据来源 ， 是语音 还是 在线客服
	private String dataid ;			//数据来源ID，通话ID或在线客服ID
	
	private String organ ;
	
	private String marriage;
	
	private String entcusid;	//客户ID
	
	private String education;
	private String identifytype;
	private String identifynumber;
	private String website;
	private String email;
	private String emailalt;
	private String mobileno;
	private String mobilealt;
	private String phone;
	private String extension;
	private String phonealt;
	private String extensionalt;
	private String familyphone;
	private String familyphonealt;
	private String fax;
	private String faxalt;
	private String country;
	private String province;
	private String city;
	private String address;
	private String postcode;
	private String enterpriseid;
	private String company;
	private String department;
	private String duty;
	private String deptpr;
	private String validstatus;
	private String weixin;
	private String weixinname;
	private String weixinid;
	private String weibo;
	private String weiboid;
	private String qqcode;
	
	private Date touchtime;
	private boolean datastatus;
	private String processid;
	private String creater;
	private String username;
	private String updateuser;
	private String memo;
	private String updateusername;
	
	private Date updatetime = new Date();
	private String orgi;
	private String compper;
	
	private Date createtime = new Date();
	private String name;
	private String assignedto;
	private String wfstatus;
	private String shares;
	private String owner;
	private String datadept;
	
	private String pinyin ;		//拼音首字母
	
	private String itemid;
	
	private String cusname ;//客户名称
	private String cusphone ;//客户-联系电话
	private String cusemail ;//客户-电子邮件
	private String cusprovince ;//客户-省份
	private String cuscity ;//客户-城市
	private String cusaddress ;//客户-客户地址
	private String ekind ;//客户-客户类型
	private String elevel ;//客户-客户级别
	private String esource ;//客户-客户来源
	private String maturity ;//客户-成熟度
	private String industry ;//客户-行业
	private String cusvalidstatus ;//客户-客户状态
	private String cusdescription ;//客户-客户说明
	
	private String workstatus ;//success failed
	private String callstatus ;//拨打状态 true false
	private String callresult ;//拨打结果 success failed
	private long calltime ;//最后一次拨打时间
	private long firstcalltimes ;//第一次拨打时间
	private String firstcallstatus ;//第一次拨打状态
	private long calltimes ;//拨打次数
	private long ringtime ;//拨打时长
	private String apstatus ;//
	private long succcall ;//成功拨打次数
	private long faildcall ;//失败拨打次数
	private long incall ;//通话时长
	private String status ;//分配状态
	private String owneruser ;	//分配 坐席
	private String ownerdept ;	//分配 部门
	private String ownerai ;	//分配 机器人
	private String ownerforecast ;	//分配 队列
	private Date distime;//分配时间
	private String actid ;		//活动ID
	private int discount;//分配次数
	
	private boolean reservation ;	//是否预约
	private String reservtype ;//预约方式
	private String salestatus;//业务状态
	private Date optime ;//预约时间
	private String salesmemo ;		//备注
	
	private boolean processed ;	//预约已调度
	
	private boolean attachment ;	//附件
	
	private String eventid;//最近一次通话记录ID
	private String hangupcase ;		//挂断原因	
	
	private String business ;//业务需求类型
	private String busmemo ;//业务需求说明
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCbirthday() {
		return cbirthday;
	}
	public void setCbirthday(String cbirthday) {
		this.cbirthday = cbirthday;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public String getCkind() {
		return ckind;
	}
	public void setCkind(String ckind) {
		this.ckind = ckind;
	}
	public String getClevel() {
		return clevel;
	}
	public void setClevel(String clevel) {
		this.clevel = clevel;
	}
	public String getCcode() {
		return ccode;
	}
	public void setCcode(String ccode) {
		this.ccode = ccode;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getSarea() {
		return sarea;
	}
	public void setSarea(String sarea) {
		this.sarea = sarea;
	}
	public String getCsource() {
		return csource;
	}
	public void setCsource(String csource) {
		this.csource = csource;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getMarriage() {
		return marriage;
	}
	public void setMarriage(String marriage) {
		this.marriage = marriage;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getIdentifytype() {
		return identifytype;
	}
	public void setIdentifytype(String identifytype) {
		this.identifytype = identifytype;
	}
	public String getIdentifynumber() {
		return identifynumber;
	}
	public void setIdentifynumber(String identifynumber) {
		this.identifynumber = identifynumber;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailalt() {
		return emailalt;
	}
	public void setEmailalt(String emailalt) {
		this.emailalt = emailalt;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getMobilealt() {
		return mobilealt;
	}
	public void setMobilealt(String mobilealt) {
		this.mobilealt = mobilealt;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getPhonealt() {
		return phonealt;
	}
	public void setPhonealt(String phonealt) {
		this.phonealt = phonealt;
	}
	public String getExtensionalt() {
		return extensionalt;
	}
	public void setExtensionalt(String extensionalt) {
		this.extensionalt = extensionalt;
	}
	public String getFamilyphone() {
		return familyphone;
	}
	public void setFamilyphone(String familyphone) {
		this.familyphone = familyphone;
	}
	public String getFamilyphonealt() {
		return familyphonealt;
	}
	public void setFamilyphonealt(String familyphonealt) {
		this.familyphonealt = familyphonealt;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getFaxalt() {
		return faxalt;
	}
	public void setFaxalt(String faxalt) {
		this.faxalt = faxalt;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getEnterpriseid() {
		return enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid) {
		this.enterpriseid = enterpriseid;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getDeptpr() {
		return deptpr;
	}
	public void setDeptpr(String deptpr) {
		this.deptpr = deptpr;
	}
	public String getValidstatus() {
		return validstatus;
	}
	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}
	public String getWeixin() {
		return weixin;
	}
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	public String getWeixinname() {
		return weixinname;
	}
	public void setWeixinname(String weixinname) {
		this.weixinname = weixinname;
	}
	public String getWeixinid() {
		return weixinid;
	}
	public void setWeixinid(String weixinid) {
		this.weixinid = weixinid;
	}
	public String getWeibo() {
		return weibo;
	}
	public void setWeibo(String weibo) {
		this.weibo = weibo;
	}
	public String getWeiboid() {
		return weiboid;
	}
	public void setWeiboid(String weiboid) {
		this.weiboid = weiboid;
	}
	public String getQqcode() {
		return qqcode;
	}
	public void setQqcode(String qqcode) {
		this.qqcode = qqcode;
	}
	public Date getTouchtime() {
		return touchtime;
	}
	public void setTouchtime(Date touchtime) {
		this.touchtime = touchtime;
	}
	public boolean isDatastatus() {
		return datastatus;
	}
	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}
	public String getProcessid() {
		return processid;
	}
	public void setProcessid(String processid) {
		this.processid = processid;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUpdateuser() {
		return updateuser;
	}
	public void setUpdateuser(String updateuser) {
		this.updateuser = updateuser;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getUpdateusername() {
		return updateusername;
	}
	public void setUpdateusername(String updateusername) {
		this.updateusername = updateusername;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCompper() {
		return compper;
	}
	public void setCompper(String compper) {
		this.compper = compper;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAssignedto() {
		return assignedto;
	}
	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}
	public String getWfstatus() {
		return wfstatus;
	}
	public void setWfstatus(String wfstatus) {
		this.wfstatus = wfstatus;
	}
	public String getShares() {
		return shares;
	}
	public void setShares(String shares) {
		this.shares = shares;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getDatadept() {
		return datadept;
	}
	public void setDatadept(String datadept) {
		this.datadept = datadept;
	}
	public String getEntcusid() {
		return entcusid;
	}
	public void setEntcusid(String entcusid) {
		this.entcusid = entcusid;
	}
	public String getOrgan() {
		return organ;
	}
	public void setOrgan(String organ) {
		this.organ = organ;
	}
	public String getPinyin() {
		return pinyin;
	}
	public void setPinyin(String pinyin) {
		this.pinyin = pinyin;
	}
	public String getSourcetype() {
		return sourcetype;
	}
	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public String getCusname() {
		return cusname;
	}
	public void setCusname(String cusname) {
		this.cusname = cusname;
	}
	public String getCusphone() {
		return cusphone;
	}
	public void setCusphone(String cusphone) {
		this.cusphone = cusphone;
	}
	public String getCusemail() {
		return cusemail;
	}
	public void setCusemail(String cusemail) {
		this.cusemail = cusemail;
	}
	public String getCusprovince() {
		return cusprovince;
	}
	public void setCusprovince(String cusprovince) {
		this.cusprovince = cusprovince;
	}
	public String getCuscity() {
		return cuscity;
	}
	public void setCuscity(String cuscity) {
		this.cuscity = cuscity;
	}
	public String getCusaddress() {
		return cusaddress;
	}
	public void setCusaddress(String cusaddress) {
		this.cusaddress = cusaddress;
	}
	public String getEkind() {
		return ekind;
	}
	public void setEkind(String ekind) {
		this.ekind = ekind;
	}
	public String getElevel() {
		return elevel;
	}
	public void setElevel(String elevel) {
		this.elevel = elevel;
	}
	public String getEsource() {
		return esource;
	}
	public void setEsource(String esource) {
		this.esource = esource;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getCusvalidstatus() {
		return cusvalidstatus;
	}
	public void setCusvalidstatus(String cusvalidstatus) {
		this.cusvalidstatus = cusvalidstatus;
	}
	public String getCusdescription() {
		return cusdescription;
	}
	public void setCusdescription(String cusdescription) {
		this.cusdescription = cusdescription;
	}
	public String getWorkstatus() {
		return workstatus;
	}
	public void setWorkstatus(String workstatus) {
		this.workstatus = workstatus;
	}
	public String getCallstatus() {
		return callstatus;
	}
	public void setCallstatus(String callstatus) {
		this.callstatus = callstatus;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getDistime() {
		return distime;
	}
	public void setDistime(Date distime) {
		this.distime = distime;
	}
	public String getOwneruser() {
		return owneruser;
	}
	public void setOwneruser(String owneruser) {
		this.owneruser = owneruser;
	}
	public String getOwnerdept() {
		return ownerdept;
	}
	public void setOwnerdept(String ownerdept) {
		this.ownerdept = ownerdept;
	}
	public String getOwnerai() {
		return ownerai;
	}
	public void setOwnerai(String ownerai) {
		this.ownerai = ownerai;
	}
	public String getOwnerforecast() {
		return ownerforecast;
	}
	public void setOwnerforecast(String ownerforecast) {
		this.ownerforecast = ownerforecast;
	}
	public String getActid() {
		return actid;
	}
	public void setActid(String actid) {
		this.actid = actid;
	}
	public String getCallresult() {
		return callresult;
	}
	public void setCallresult(String callresult) {
		this.callresult = callresult;
	}
	public long getCalltime() {
		return calltime;
	}
	public void setCalltime(long calltime) {
		this.calltime = calltime;
	}
	public long getFirstcalltimes() {
		return firstcalltimes;
	}
	public void setFirstcalltimes(long firstcalltimes) {
		this.firstcalltimes = firstcalltimes;
	}
	public String getFirstcallstatus() {
		return firstcallstatus;
	}
	public void setFirstcallstatus(String firstcallstatus) {
		this.firstcallstatus = firstcallstatus;
	}
	public long getCalltimes() {
		return calltimes;
	}
	public void setCalltimes(long calltimes) {
		this.calltimes = calltimes;
	}
	public long getRingtime() {
		return ringtime;
	}
	public void setRingtime(long ringtime) {
		this.ringtime = ringtime;
	}
	public String getApstatus() {
		return apstatus;
	}
	public void setApstatus(String apstatus) {
		this.apstatus = apstatus;
	}
	public long getSucccall() {
		return succcall;
	}
	public void setSucccall(long succcall) {
		this.succcall = succcall;
	}
	public long getFaildcall() {
		return faildcall;
	}
	public void setFaildcall(long faildcall) {
		this.faildcall = faildcall;
	}
	public long getIncall() {
		return incall;
	}
	public void setIncall(long incall) {
		this.incall = incall;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public boolean isReservation() {
		return reservation;
	}
	public void setReservation(boolean reservation) {
		this.reservation = reservation;
	}
	public String getSalestatus() {
		return salestatus;
	}
	public void setSalestatus(String salestatus) {
		this.salestatus = salestatus;
	}
	public Date getOptime() {
		return optime;
	}
	public void setOptime(Date optime) {
		this.optime = optime;
	}
	public String getSalesmemo() {
		return salesmemo;
	}
	public void setSalesmemo(String salesmemo) {
		this.salesmemo = salesmemo;
	}
	public boolean isAttachment() {
		return attachment;
	}
	public void setAttachment(boolean attachment) {
		this.attachment = attachment;
	}
	public boolean isProcessed() {
		return processed;
	}
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
	public String getEventid() {
		return eventid;
	}
	public void setEventid(String eventid) {
		this.eventid = eventid;
	}
	public String getHangupcase() {
		return hangupcase;
	}
	public void setHangupcase(String hangupcase) {
		this.hangupcase = hangupcase;
	}
	public String getReservtype() {
		return reservtype;
	}
	public void setReservtype(String reservtype) {
		this.reservtype = reservtype;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getBusmemo() {
		return busmemo;
	}
	public void setBusmemo(String busmemo) {
		this.busmemo = busmemo;
	}
}
