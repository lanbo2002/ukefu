package com.ukefu.webim.service.acd;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.hazelcast.aggregation.Aggregators;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.SqlPredicate;
import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.util.WebIMReport;
import com.ukefu.util.client.NettyClients;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.es.OnlineUserESRepository;
import com.ukefu.webim.service.impl.AgentUserService;
import com.ukefu.webim.service.quene.AgentStatusBusyOrgiFilterPredicate;
import com.ukefu.webim.service.quene.AgentStatusOrgiFilterPredicate;
import com.ukefu.webim.service.quene.AgentUserIndexPredicate;
import com.ukefu.webim.service.quene.AgentUserOrgiFilterPredicate;
import com.ukefu.webim.service.repository.AgentReportRepository;
import com.ukefu.webim.service.repository.AgentServiceRepository;
import com.ukefu.webim.service.repository.AgentStatusRepository;
import com.ukefu.webim.service.repository.AgentUserRepository;
import com.ukefu.webim.service.repository.AgentUserTaskRepository;
import com.ukefu.webim.service.repository.OnlineUserRepository;
import com.ukefu.webim.service.repository.SessionConfigRepository;
import com.ukefu.webim.service.repository.WorkMonitorRepository;
import com.ukefu.webim.util.MessageUtils;
import com.ukefu.webim.util.router.OutMessageRouter;
import com.ukefu.webim.web.model.AgentReport;
import com.ukefu.webim.web.model.AgentService;
import com.ukefu.webim.web.model.AgentStatus;
import com.ukefu.webim.web.model.AgentUser;
import com.ukefu.webim.web.model.AgentUserTask;
import com.ukefu.webim.web.model.AiUser;
import com.ukefu.webim.web.model.MessageOutContent;
import com.ukefu.webim.web.model.OnlineUser;
import com.ukefu.webim.web.model.SessionConfig;
import com.ukefu.webim.web.model.WorkMonitor;

public class ServiceQuene {
	
	/**
	 * 载入坐席 ACD策略配置
	 * @param orgi
	 * @return
	 */
	public static SessionConfig initSessionConfig(String orgi){
		SessionConfig sessionConfig = null;
		if(UKDataContext.getContext() != null && (sessionConfig = (SessionConfig) CacheHelper.getSystemCacheBean().getCacheObject(UKDataContext.SYSTEM_CACHE_SESSION_CONFIG+"_"+orgi, orgi)) == null){
			SessionConfigRepository agentUserRepository = UKDataContext.getContext().getBean(SessionConfigRepository.class) ;
			sessionConfig = agentUserRepository.findByOrgi(orgi) ;
			if(sessionConfig != null){
				CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_SESSION_CONFIG+"_"+orgi,sessionConfig, orgi) ;
			}
		}
		if(sessionConfig == null) {
			sessionConfig = new SessionConfig() ;
		}
		return sessionConfig ;
	}
	
	/**
	 * 载入坐席 ACD策略配置
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<SessionConfig> initSessionConfigList(){
		List<SessionConfig> sessionConfigList = null;
		if(UKDataContext.getContext() != null && (sessionConfigList = (List<SessionConfig>) CacheHelper.getSystemCacheBean().getCacheObject(UKDataContext.SYSTEM_CACHE_SESSION_CONFIG_LIST, UKDataContext.SYSTEM_ORGI)) == null){
			SessionConfigRepository agentUserRepository = UKDataContext.getContext().getBean(SessionConfigRepository.class) ;
			sessionConfigList = agentUserRepository.findAll();
			if(sessionConfigList != null && sessionConfigList.size() > 0){
				CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_SESSION_CONFIG_LIST,sessionConfigList, UKDataContext.SYSTEM_ORGI) ;
			}
		}
		return sessionConfigList ;
	}
	/**
	 * 获得 当前服务状态
	 * @param orgi
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static AgentReport getAgentReport(String orgi){
		/**
		 * 统计当前在线的坐席数量
		 */
		AgentReport report = new AgentReport() ;
		IMap agentStatusMap = (IMap<String, Object>) CacheHelper.getAgentStatusCacheBean().getCache() ;
		Long agents = (Long) agentStatusMap.aggregate(Aggregators.<Map.Entry<String, AgentStatus>>count(), new AgentStatusOrgiFilterPredicate(orgi)) ;
		report.setAgents(agents.intValue());
		
		Long busyAgent = (Long) agentStatusMap.aggregate(Aggregators.<Map.Entry<String, AgentStatus>>count(), new AgentStatusBusyOrgiFilterPredicate(orgi)) ;
		report.setBusy(busyAgent.intValue());
		report.setOrgi(orgi);
		
		/**
		 * 统计当前服务中的用户数量
		 */
		IMap agentUserMap = (IMap<String, Object>) CacheHelper.getAgentUserCacheBean().getCache() ;
		Long users = (Long) agentUserMap.aggregate(Aggregators.<Map.Entry<String, AgentUser>>count(), new AgentUserOrgiFilterPredicate(orgi,UKDataContext.AgentUserStatusEnum.INSERVICE.toString())) ;
		report.setUsers(users.intValue());
		
		Long queneUsers = (Long) agentUserMap.aggregate(Aggregators.<Map.Entry<String, AgentUser>>count(), new AgentUserOrgiFilterPredicate(orgi,UKDataContext.AgentUserStatusEnum.INQUENE.toString())) ;
		report.setInquene(queneUsers.intValue());
		
		return report;
	}
	
	@SuppressWarnings("unchecked")
	public static int getQueneIndex(String userid , String orgi , long ordertime){
		
		IMap<String,AgentUser> agentUserMap = (IMap<String,AgentUser>) CacheHelper.getAgentUserCacheBean().getCache() ;
		Long queneUsers = (Long) agentUserMap.aggregate(Aggregators.<Map.Entry<String, AgentUser>>count(), new AgentUserIndexPredicate(orgi,UKDataContext.AgentUserStatusEnum.INQUENE.toString() , ordertime)) ;

		return queneUsers!=null ? queneUsers.intValue() : 0;
	}
	
	@SuppressWarnings("unchecked")
	public static int getQueneIndex(String agent , String orgi , String skill){
		
		int queneUsers = 0 ;
		
		PagingPredicate<String, AgentUser> pagingPredicate = null ;
		if(!StringUtils.isBlank(skill)){
			pagingPredicate = new PagingPredicate<String, AgentUser>(  new SqlPredicate( "status = 'inquene' AND skill = '" + skill + "'  AND orgi = '" + orgi +"'") , 100 );
		}else if(!StringUtils.isBlank(agent)){
			pagingPredicate = new PagingPredicate<String, AgentUser>(  new SqlPredicate( "status = 'inquene' AND agent = '"+agent+"' AND orgi = '" + orgi +"'") , 100 );
		}else{
			pagingPredicate = new PagingPredicate<String, AgentUser>(  new SqlPredicate( "status = 'inquene' AND orgi = '" + orgi +"'") , 100 );
		}
		queneUsers = ((IMap<String , AgentUser>) CacheHelper.getAgentUserCacheBean().getCache()).values(pagingPredicate) .size();
		return queneUsers;
	}
	
	public static int getAgentUsers(String agent , String orgi){
		/**
		 * agentno自动是 服务的坐席， agent 是请求的坐席
		 */
		Long users = UKDataContext.getContext().getBean(OnlineUserRepository.class).countByAgentno( orgi , agent); 
		return users!=null ? users.intValue() : 0;
	}
	

	@SuppressWarnings("unchecked")
	public static List<AgentStatus> getAgentStatus(String skill , String orgi){
		PagingPredicate<String, AgentStatus> pagingPredicate = new PagingPredicate<String, AgentStatus>(  new SqlPredicate( "orgi = '" + orgi +"'") , 100 ) ;
		
		if(!StringUtils.isBlank(skill)) {
			pagingPredicate = new PagingPredicate<String, AgentStatus>(  new SqlPredicate( "skill = '"+skill+"' AND orgi = '" + orgi +"'") , 100 ) ;
		}
		List<AgentStatus> agentList = new ArrayList<AgentStatus>();
		agentList.addAll(((IMap<String , AgentStatus>) CacheHelper.getAgentStatusCacheBean().getCache()).values(pagingPredicate)) ;
		return agentList;
	}
	/**
	 * 为坐席批量分配用户
	 * @param agentStatus
	 */
	@SuppressWarnings("unchecked")
	public static void allotAgent(String agentno , String orgi){
		AgentStatus agentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentno, orgi) ;
		SessionConfig sessionConfig = ServiceQuene.initSessionConfig(orgi) ;
		long maxusers = sessionConfig!=null ? sessionConfig.getMaxuser() : UKDataContext.AGENT_STATUS_MAX_USER ;
		if(agentStatus!=null && agentStatus.isBusy() == false) {
			List<AgentUser> agentStatusList = new ArrayList<AgentUser>();
			agentStatus.setUsers(getAgentUsers(agentStatus.getAgentno(), orgi));
			
			/**
			 * 以下代码处理已在当前坐席会话列表中的访客直接接入，无需等待
			 */
			AgentUserRepository agentUserRepository = UKDataContext.getContext().getBean(AgentUserRepository.class) ;
			List<AgentUser> tempAgentUserList = agentUserRepository.findByAgentnoAndOrgi(agentno, orgi, new Sort(Direction.DESC,"status")) ;
			if(tempAgentUserList.size() > 0) {
				for(AgentUser agentUser : tempAgentUserList) {
					agentUser = (AgentUser) CacheHelper.getAgentUserCacheBean().getCacheObject(agentUser.getUserid(), orgi) ;
					if(agentUser!=null && UKDataContext.AgentUserStatusEnum.INQUENE.toString().equals(agentUser.getStatus())) {
						processAllotUser(agentStatus, maxusers, agentUser, orgi);
					}
				}
			}
			/** 结束 **/
			
			/**
			 * 以下代码处理不在坐席排队列表中的访客接入，需要做计数
			 */
			if(maxuser(agentStatus, maxusers)){		//坐席未达到最大咨询访客数量
				/**
				 * 还需要再分配多少访客
				 */
				int leaveusers = getLiveusers(agentStatus, (int)maxusers) ;
				if(leaveusers > 0) {
					PagingPredicate<String, AgentUser> pagingPredicate = null ;
					if(agentStatus!=null && !StringUtils.isBlank(agentStatus.getSkill())){
						pagingPredicate = new PagingPredicate<String, AgentUser>(  new SqlPredicate( "status = 'inquene' AND ((agent = null AND skill = null) OR (skill = '" + agentStatus.getSkill() + "' AND agent = null) OR agent = '"+agentno+"') AND orgi = '" + orgi +"'") , leaveusers );
					}else{
						pagingPredicate = new PagingPredicate<String, AgentUser>(  new SqlPredicate( "status = 'inquene' AND ((agent = null AND skill = null) OR agent = '"+agentno+"') AND orgi = '" + orgi +"'") , leaveusers );
					}
					agentStatusList.addAll(((IMap<String , AgentUser>) CacheHelper.getAgentUserCacheBean().getCache()).values(pagingPredicate)) ;
				}
			}
			if(agentStatusList.size()>0) {
				for(AgentUser agentUser : agentStatusList){
					if(maxuser(agentStatus, maxusers)){		//坐席未达到最大咨询访客数量
						processAllotUser(agentStatus, maxusers, agentUser, orgi);
					}else {
						break ;
					}
				}
			}
		}
		publishMessage(orgi , "agent" , "success" , agentno);
	}
	
	private static void processAllotUser(AgentStatus agentStatus , long maxusers , AgentUser agentUser , String orgi) {
		/**
		 * 以下代码 负责处理 分配最大数
		 * 1、如果坐席的 maxuser == 0， 则以默认配置的最大数作为上限
		 * 2、如果坐席的 maxuser != 0， 则以配置的maxuser作为上限
		 */
		try{
			AgentService agentService = processAgentService(agentStatus, agentUser, orgi) ;

			MessageOutContent outMessage = new MessageOutContent() ;
			outMessage.setMessage(ServiceQuene.getSuccessMessage(agentService , agentUser.getChannel(),orgi));
			outMessage.setMessageType(UKDataContext.MediaTypeEnum.TEXT.toString());
			outMessage.setCalltype(UKDataContext.CallTypeEnum.IN.toString());
			outMessage.setNickName(agentStatus.getUsername());
			outMessage.setCreatetime(UKTools.dateFormate.format(new Date()));

			if(!StringUtils.isBlank(agentUser.getUserid())){
				OutMessageRouter router = null ; 
				router  = (OutMessageRouter) UKDataContext.getContext().getBean(agentUser.getChannel()) ;
				if(router!=null){
					router.handler(agentUser.getUserid(), UKDataContext.MessageTypeEnum.STATUS.toString(), agentUser.getAppid(), outMessage);
				}
			}

			NettyClients.getInstance().sendAgentEventMessage(agentService.getAgentno(), UKDataContext.MessageTypeEnum.NEW.toString(), agentUser);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		/**
		 * 更新当前坐席的 接入访客数量
		 */
		agentStatus.setUsers(getAgentUsers(agentStatus.getAgentno(), orgi));
	}
	
	/**
	 * 为坐席批量分配用户
	 * @param agentStatus
	 * @throws Exception 
	 */
	public static void serviceFinish(AgentUser agentUser , String orgi , String endby , boolean delete) throws Exception{
		SessionConfig sessionConfig = ServiceQuene.initSessionConfig(orgi) ;
		if(agentUser!=null){
			AgentStatus agentStatus = null;
			if(UKDataContext.AgentUserStatusEnum.INSERVICE.toString().equals(agentUser.getStatus()) && agentUser.getAgentno()!=null){
				agentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentUser.getAgentno(), orgi) ;
			}
			CacheHelper.getAgentUserCacheBean().delete(agentUser.getUserid(), orgi);
			
			AgentServiceRepository agentServiceRes = UKDataContext.getContext().getBean(AgentServiceRepository.class) ;
			AgentService service = 	null ;
			if(!StringUtils.isBlank(agentUser.getAgentserviceid())){
				service = 	agentServiceRes.findByIdAndOrgi(agentUser.getAgentserviceid() , agentUser.getOrgi()) ;
			}
			
			if (!UKDataContext.AgentUserStatusEnum.END.toString().equals(
					agentUser.getStatus())) {
				AgentUserRepository agentUserRepository = UKDataContext.getContext().getBean(AgentUserRepository.class) ;
				
				AgentUser agentUseDataBean = agentUserRepository.findByIdAndOrgi(agentUser.getId() , agentUser.getOrgi()) ;
				if(agentUseDataBean!=null){
					agentUseDataBean.setEndby(endby);
					agentUseDataBean.setStatus(UKDataContext.AgentUserStatusEnum.END.toString()) ;
					if(agentUser.getServicetime()!=null){
						agentUseDataBean.setSessiontimes(System.currentTimeMillis() - agentUser.getServicetime().getTime()) ;
					}
					agentUseDataBean.setEndtime(new Date());
					agentUserRepository.save(agentUseDataBean) ;
					
					/**
					 * 更新OnlineUser对象，变更为服务中，不可邀请 , WebIM渠道专用
					 */
					if(UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(agentUser.getChannel())){
						OnlineUserESRepository onlineUserRes = UKDataContext.getContext().getBean(OnlineUserESRepository.class) ;
						List<OnlineUser> onlineUserList = onlineUserRes.findByUseridAndOrgi(agentUser.getUserid() , agentUser.getOrgi());
						if (onlineUserList.size() > 0) {
							OnlineUser onlineUser = onlineUserList.get(0) ;
							onlineUser.setInvitestatus(UKDataContext.OnlineUserInviteStatus.DEFAULT.toString());
							onlineUserRes.save(onlineUser) ;
						}
					}
				}
				
				if(service == null) {//当做留言处理
					service = processAgentService(agentStatus, agentUser, orgi , true) ;
				}
				if(service!=null){
					service.setStatus(UKDataContext.AgentUserStatusEnum.END.toString());
					service.setEndtime(new Date());
					service.setEndby(endby);
					if(service.getServicetime()!=null){
						service.setSessiontimes(System.currentTimeMillis() - service.getServicetime().getTime());
					}
					AgentUserTaskRepository agentUserTaskRes = UKDataContext.getContext().getBean(AgentUserTaskRepository.class) ;
					List<AgentUserTask> agentUserTaskList = agentUserTaskRes.findByIdAndOrgi(agentUser.getId() , agentUser.getOrgi()) ;
					if(agentUserTaskList.size() > 0){
						AgentUserTask agentUserTask = agentUserTaskList.get(0) ;
						service.setAgentreplyinterval(agentUserTask.getAgentreplyinterval());
						service.setAgentreplytime(agentUserTask.getAgentreplytime());
						service.setAvgreplyinterval(agentUserTask.getAvgreplyinterval());
						service.setAvgreplytime(agentUserTask.getAvgreplytime());
						
						service.setFilteragentscript(agentUserTask.getFilteragentscript());
						service.setFilterscript(agentUserTask.getFilterscript());
	
						service.setUserasks(agentUserTask.getUserasks());
						service.setAgentreplys(agentUserTask.getAgentreplys());
						
						service.setFirstreplytime(agentUserTask.getFirstreplytime());
					}
	
					/**
					 * 启用了质检任务，开启质检
					 */
					 if(sessionConfig.isQuality() && service.getUserasks() > 0) {	//开启了质检，并且是有效对话
						 service.setQualitystatus(UKDataContext.QualityStatus.NODIS.toString());	//未分配质检任务
					 }else {
						 service.setQualitystatus(UKDataContext.QualityStatus.NO.toString());	//未开启质检 或无效对话无需质检
					 }
					 agentServiceRes.save(service) ;
				}
				
				if(agentStatus!=null){
					NettyClients.getInstance().sendAgentEventMessage(agentUser.getAgentno(), UKDataContext.MessageTypeEnum.END.toString(), agentUser);
				}
				
				OutMessageRouter router = null ; 
	    		router  = (OutMessageRouter) UKDataContext.getContext().getBean(agentUser.getChannel()) ;
	    		if(router!=null){
	    			MessageOutContent outMessage = new MessageOutContent() ;
					outMessage.setMessage(ServiceQuene.getServiceFinishMessage(agentUser.getChannel(),orgi));
					outMessage.setMessageType(UKDataContext.AgentUserStatusEnum.END.toString());
					outMessage.setCalltype(UKDataContext.CallTypeEnum.IN.toString());
					if(agentStatus!=null){
						outMessage.setNickName(agentStatus.getUsername());
					}else{
						outMessage.setNickName(agentUser.getUsername());
					}
					outMessage.setCreatetime(UKTools.dateFormate.format(new Date()));
					outMessage.setAgentserviceid(agentUser.getAgentserviceid());
					
	    			router.handler(agentUser.getUserid(), UKDataContext.MessageTypeEnum.STATUS.toString(), agentUser.getAppid(), outMessage);
	    		}
			}
			
			if(delete && !StringUtils.isBlank(agentUser.getId())) {
				AgentUserRepository agentUserRes = UKDataContext.getContext().getBean(AgentUserRepository.class) ;
				if(agentUserRes.findByIdAndOrgi(agentUser.getId(), orgi)!=null) {
					agentUserRes.delete(agentUser);
					if(service!=null) {
						MessageUtils.createStatusMessage(UKDataContext.MessageTypeEnum.END.toString(), service, service.getAiid()) ;
					}
				}
			}else if(UKDataContext.EndByType.AI.toString().equals(endby)) {
				if(service!=null) {
					MessageUtils.createStatusMessage(UKDataContext.MessageTypeEnum.END.toString(), service, service.getAiid()) ;
				}
			}
			
			
    		if(agentStatus!=null){
				updateAgentStatus(agentStatus  , agentUser , orgi , false) ;
				
				long maxusers = sessionConfig!=null ? sessionConfig.getMaxuser() : UKDataContext.AGENT_STATUS_MAX_USER ;
				if(maxuser(agentStatus, maxusers)){
					allotAgent(agentStatus.getAgentno(), orgi);
				}
			}
			publishMessage(orgi, "end" , "success" ,agentUser!=null ? agentUser.getId() : null);
		}
	}
	
	/**
	 * 更新坐席当前服务中的用户状态
	 * @param agentStatus
	 * @param agentUser
	 * @param orgi
	 */
	public synchronized static void updateAgentStatus(AgentStatus agentStatus , AgentUser agentUser , String orgi , boolean in){
		int users = getAgentUsers(agentStatus.getAgentno(), orgi) ;
		agentStatus.setUsers(users);
		agentStatus.setUpdatetime(new Date());
		AgentStatusRepository agentStatusRes = UKDataContext.getContext().getBean(AgentStatusRepository.class);
		if(agentStatusRes.findByIdAndOrgi(agentStatus.getId(), agentStatus.getOrgi())!=null) {
			CacheHelper.getAgentStatusCacheBean().put(agentStatus.getAgentno(), agentStatus, orgi);
			agentStatusRes.save(agentStatus) ;
		}else {
			CacheHelper.getAgentStatusCacheBean().delete(agentStatus.getAgentno(), orgi);
		}
	}
	
	public static void publishMessage(String orgi ,String worktype,String workresult ,String dataid){
		/**
		 * 坐席状态改变，通知监测服务
		 */
		AgentReport agentReport = ServiceQuene.getAgentReport(orgi) ;
		AgentReportRepository agentReportRes = UKDataContext.getContext().getBean(AgentReportRepository.class) ;
		if(agentReportRes!=null) {
			agentReport.setOrgi(orgi);
			agentReport.setWorktype(worktype);
			agentReport.setWorkresult(workresult);
			agentReport.setDataid(dataid);
			
			agentReportRes.save(agentReport) ;
			
			if(!StringUtils.isBlank(dataid)) {
				NettyClients.getInstance().sendAgentEventMessage(dataid, UKDataContext.MessageTypeEnum.WORKSTATUS.toString(), agentReport);
			}
		}
		NettyClients.getInstance().published("agentNamespace", "status", agentReport);
		NettyClients.getInstance().published("imNamespace", "queue", agentReport);
	}
	/**
	 * 
	 * @param agent  坐席
	 * @param skill	 技能组
	 * @param userid 用户ID  
	 * @param status	工作状态
	 * @param worktype  类型 ： 语音OR 文本
	 * @param orgi
	 * @param lasttime
	 */
	public static void recordAgentStatus(String agent,String username,String extno,String skill,boolean admin,String userid, String status ,String current ,String worktype ,String orgi , Date lasttime){
		WorkMonitorRepository workMonitorRes = UKDataContext.getContext().getBean(WorkMonitorRepository.class) ;
		WorkMonitor workMonitor = new WorkMonitor() ;
		if(!StringUtils.isBlank(agent) && !StringUtils.isBlank(status)) {
			workMonitor.setAgent(agent);
			workMonitor.setAgentno(agent);
			workMonitor.setStatus(status);
			workMonitor.setAdmin(admin);
			workMonitor.setUsername(username);
			workMonitor.setExtno(extno);
			workMonitor.setWorktype(worktype);
			if(lasttime!=null) {
				workMonitor.setDuration((int) (System.currentTimeMillis() - lasttime.getTime())/1000);
			}
			if(status.equals(UKDataContext.AgentStatusEnum.BUSY.toString())) {
				workMonitor.setBusy(true);
			}
			if(status.equals(UKDataContext.AgentStatusEnum.READY.toString())) {
				int count = workMonitorRes.countByAgentAndDatestrAndStatusAndOrgi(agent,UKTools.simpleDateFormat.format(new Date()), UKDataContext.AgentStatusEnum.READY.toString(), orgi) ;
				if(count == 0) {
					workMonitor.setFirsttime(true);
				}
			}
			if(current.equals(UKDataContext.AgentStatusEnum.NOTREADY.toString())) {
				List<WorkMonitor> workMonitorList = workMonitorRes.findByOrgiAndAgentAndDatestrAndFirsttime(orgi , agent , UKTools.simpleDateFormat.format(new Date()) , true);
				if(workMonitorList.size() > 0) {
					WorkMonitor firstWorkMonitor = workMonitorList.get(0) ;
					if(firstWorkMonitor.getFirsttimes() == 0) {
						firstWorkMonitor.setFirsttimes((int) (System.currentTimeMillis() - firstWorkMonitor.getCreatetime().getTime())/1000);
						workMonitorRes.save(firstWorkMonitor) ;
					}
				}
			}
			workMonitor.setWorkstatus(current);
			workMonitor.setCreatetime(new Date());
			workMonitor.setDatestr(UKTools.simpleDateFormat.format(new Date()));
			
			workMonitor.setName(agent);
			workMonitor.setOrgi(orgi);
			workMonitor.setSkill(skill);
			workMonitor.setUserid(userid);
			
			workMonitorRes.save(workMonitor) ;
		}
	}
	/**
	 * 为用户分配坐席
	 * @param agentUser
	 */
	public static synchronized AgentService allotAgent(AgentUser agentUser , String orgi){
		/**
		 * 坐席分配
		 */
		AgentService agentService = null ;	
		Lock lock = CacheHelper.getAgentStatusCacheBean().getLock("LOCK", orgi) ;
		/**
		 * 查询条件，当前在线的 坐席，并且 未达到最大 服务人数的坐席
		 */
		try {
			if(lock!=null) {
				lock.lock();
			}
			SessionConfig config = initSessionConfig(orgi) ;
			List<AgentStatus> agentStatusList = new ArrayList<AgentStatus>();
			AgentStatus agentStatus = null ;
			/**
			 * 处理ACD 的 技能组请求和 坐席请求
			 */
			if(!StringUtils.isBlank(agentUser.getAgentno())){
				AgentStatus agentStatusTemp = agentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentUser.getAgentno(), agentUser.getOrgi());
				if(agentStatusTemp != null && config.isEnablersession() ){
					agentStatusList.add((AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentUser.getAgentno(), agentUser.getOrgi())) ;
				}else{
					agentUser.setAgentno(null);
					AgentUserService service = UKDataContext.getContext().getBean(
							AgentUserService.class);
					service.save(agentUser);
					if(agentStatusTemp != null && !config.isEnablersession() ){
						updateAgentStatus(agentStatusTemp, agentUser, orgi, false);
					}
				}			
			}
			/**
			 * 以下是坐席分配代码
			 */
			if(agentStatusList.size() == 0) {
				OnlineUserRepository onlineUserRes  = UKDataContext.getContext().getBean(OnlineUserRepository.class);
				AgentStatusRepository agentStatusRes = UKDataContext.getContext().getBean(AgentStatusRepository.class);
				
				List<Object> tempStatusList = null ;
				
				String field = "users" ;	//分配方式 （按空闲坐席分配|平均分配）
				if(config!=null && !StringUtils.isBlank(config.getDistribution()) && config.getDistribution().equals("1")) {
					field = "updatetime" ;
				}
				/**
				 * 访客选择了坐席 , 但是如果坐席不在线， 需要 移除掉坐席 字段信息，不然会出现 访客在排队中无法接入服务的问题
				 */
				if(!StringUtils.isBlank(agentUser.getAgent()) && CacheHelper.getAgentStatusCacheBean().getCacheObject(agentUser.getAgent(), agentUser.getOrgi()) == null) {
					agentUser.setAgent(null);
				}
				/**
				 * 处理完成以后，后续进入正常的排队分配流程
				 */
				if(!StringUtils.isBlank(agentUser.getAgent())){
					tempStatusList = onlineUserRes.findAgentByAgentnoAndOrgi( agentUser.getAgent() , orgi , config.getMaxuser() , field) ;
				}else if(!StringUtils.isBlank(agentUser.getSkill())){//请求了技能组
					tempStatusList = onlineUserRes.findAgentBySkillAndOrgi( agentUser.getSkill() , orgi, config.getMaxuser(), field) ;
				}else{//默认分配
					tempStatusList = onlineUserRes.findAgentByOrgi(orgi, config.getMaxuser(), field) ;
				}
				while(tempStatusList.size() > 0) {
					Object[] temp = (Object[]) tempStatusList.remove(0) ;
					AgentStatus tempAgentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject((String) temp[0], orgi)  ;
					if(tempAgentStatus == null) {
						agentStatusRes.delete((String) temp[0]);
					}else {
						agentStatusList.add(tempAgentStatus);
						break ;
					}
				}
			}
			/**
			 * 找到了分配对象（包括两种请求，1、坐席分配成功；2、访客之前的服务未结束，找到了之前服务的坐席（非历史坐席优先））
			 */
			if(agentStatusList!=null && agentStatusList.size() > 0){
				AgentStatus agent = agentStatusList.get(0) ;
				if(agent!=null) {
					agent.setUsers(getAgentUsers(agent.getAgentno(), orgi));
					if(maxuser(agent, config.getMaxuser())) {
						agentStatus = agent ;
					}
				}
			}
			/**
			 * 	当前分配的坐席和之前访客的服务坐席不是同一个人的情况下，需要通知之前的坐席，在浏览器端执行清理动作
			 */
			if(agentStatus!=null && !StringUtils.isBlank(agentUser.getAgentno()) && !agentStatus.getAgentno().equals(agentUser.getAgentno())) {
				NettyClients.getInstance().sendAgentEventMessage(agentUser.getAgentno(), UKDataContext.MessageTypeEnum.EXCHANGE.toString(), agentUser);
			}
			/**
			 * 创建服务请求信息
			 */
			agentService = processAgentService(agentStatus, agentUser, orgi) ;
			
			/**
			 * 未成功分配坐席，访客进入了队列，通知访客，当前排队在第几个（修改成数据库执行ACD操作，即可实时知道当前访客排在第几个，并进行相应通知）
			 */
			if(agentService!=null && agentService.getStatus().equals(UKDataContext.AgentUserStatusEnum.INQUENE.toString())){
				agentService.setQueneindex(getQueneIndex(agentUser.getAgent(), orgi, agentUser.getSkill()));
			}
			
		}catch(Exception ex){
			ex.printStackTrace(); 
		}finally{
			if(lock!=null) {
				lock.unlock();
			}
		}
		/**
		 * 发送公告消息
		 */
		publishMessage(orgi, "user" , agentService!=null && agentService.getStatus().equals(UKDataContext.AgentUserStatusEnum.INSERVICE.toString()) ? "inservice" : "inquene" , agentUser.getId());
		return agentService;
	}
	/**
	 * 判断是否需要分配
	 * @param agentStatus
	 * @param configMaxUser
	 * @return
	 */
	private static boolean maxuser(AgentStatus agentStatus , long configMaxUser) {
		boolean ok = false ; 
		if(agentStatus.getMaxusers() == 0) {
			ok = agentStatus.getUsers() < configMaxUser ;
		}else {
			ok = agentStatus.getUsers() < agentStatus.getMaxusers() ;
		}
		return ok ;
	}
	
	/**
	 * 判断是否需要分配
	 * @param agentStatus
	 * @param configMaxUser
	 * @return
	 */
	private static int getLiveusers(AgentStatus agentStatus , int configMaxUser) {
		int ok = 0 ; 
		if(agentStatus.getMaxusers() == 0) {
			ok = configMaxUser - agentStatus.getUsers();
		}else {
			ok = agentStatus.getMaxusers() - agentStatus.getUsers() ;
		}
		return ok > 0 ? ok : 0 ;
	}
	
	/**
	 * 邀请访客进入当前对话，如果当前操作的 坐席是已就绪状态，则直接加入到当前坐席的 对话列表中，如果未登录，则分配给其他坐席
	 * @param agentno
	 * @param agentUser
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	public static AgentService allotAgentForInvite(String agentno , AgentUser agentUser , String orgi) throws Exception{
		AgentStatus agentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(agentno, orgi) ;
		AgentService agentService = null ;
		if(agentStatus!=null){
			agentService = processAgentService(agentStatus, agentUser, orgi) ;
			publishMessage(orgi , "invite" , "success" , agentno);
			NettyClients.getInstance().sendAgentEventMessage(agentService.getAgentno(), UKDataContext.MessageTypeEnum.NEW.toString(), agentUser);
		}else{
			agentService = allotAgent(agentUser, orgi) ;
		}
		return agentService;
	}
	
	/**
	 * 专用于转接操作
	 * @param agentUser
	 */
	public static void processAgentUser(AgentUser agentUser) {
		if(agentUser!=null) {
			AgentUserRepository agentUserRepository = UKDataContext.getContext().getBean(AgentUserRepository.class) ;
			/**
			 * 清空数据
			 */
			agentUser.setTransfer(false);
			agentUser.setAgentno(null);
			agentUser.setTransferagent(null);
			agentUser.setTransfertime(null);
			agentUser.setStatus(UKDataContext.AgentUserStatusEnum.INQUENE.toString());
			
			agentUserRepository.save(agentUser) ;
			
			CacheHelper.getAgentUserCacheBean().put(agentUser.getUserid() , agentUser , agentUser.getOrgi()) ;
			
			AgentService agentService = ServiceQuene.allotAgent(agentUser, agentUser.getOrgi()) ;
			if(agentService!=null && UKDataContext.AgentUserStatusEnum.INSERVICE.toString().equals(agentService.getStatus())) {
				NettyClients.getInstance().sendAgentEventMessage(agentService.getAgentno(), UKDataContext.MessageTypeEnum.NEW.toString(), agentUser);
			}
		}
	}
	
	/**
	 * 为访客 分配坐席， ACD策略，此处 AgentStatus 是建议 的 坐席，  如果启用了  历史服务坐席 优先策略， 则会默认检查历史坐席是否空闲，如果空闲，则分配，如果不空闲，则 分配当前建议的坐席
	 * @param agentStatus
	 * @param agentUser
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	private static AgentService processAgentService(AgentStatus agentStatus , AgentUser agentUser , String orgi) throws Exception{
		return processAgentService(agentStatus, agentUser, orgi , false) ;
	}
	
	/**
	 * 为访客 分配坐席， ACD策略，此处 AgentStatus 是建议 的 坐席，  如果启用了  历史服务坐席 优先策略， 则会默认检查历史坐席是否空闲，如果空闲，则分配，如果不空闲，则 分配当前建议的坐席
	 * @param agentStatus
	 * @param agentUser
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	public static AgentService processAiService(AiUser aiUser , String orgi ,String endby) throws Exception{
		AgentService agentService = new AgentService();	//放入缓存的对象
		AgentServiceRepository agentServiceRes  = UKDataContext.getContext().getBean(AgentServiceRepository.class);
		AgentService temp  = null ;
		if(!StringUtils.isBlank(aiUser.getAgentserviceid()) && (temp = agentServiceRes.findByIdAndOrgi(aiUser.getAgentserviceid(), orgi))!=null) {
			agentService =  temp ;
			agentService = agentServiceRes.findByIdAndOrgi(aiUser.getAgentserviceid(), orgi) ;
			agentService.setServicetime(new Date());
			agentService.setEndtime(new Date());
			if(agentService.getServicetime()!=null) {
				agentService.setSessiontimes(System.currentTimeMillis() - agentService.getServicetime().getTime());
			}
			agentService.setUserasks(aiUser.getUserask());
			agentService.setAgentreplys(aiUser.getAireply());
			
			if(aiUser.getIpdata()!=null) {
				agentService.setRegion(aiUser.getIpdata().getRegion());
				agentService.setCity(aiUser.getIpdata().getCity());
				agentService.setProvince(aiUser.getIpdata().getProvince());
				agentService.setCountry(aiUser.getIpdata().getCountry());
			}
			agentService.setIpaddr(aiUser.getIp());
			agentService.setStatus(UKDataContext.AgentUserStatusEnum.END.toString());
		}else {
			agentService.setServicetime(new Date());
			agentService.setLogindate(new Date());
			agentService.setOrgi(orgi);
			agentService.setOwner(aiUser.getContextid());
			agentService.setSessionid(aiUser.getSessionid());
			if(aiUser.getIpdata()!=null) {
				agentService.setRegion(aiUser.getIpdata().getRegion());
				agentService.setCity(aiUser.getIpdata().getCity());
				agentService.setProvince(aiUser.getIpdata().getProvince());
				agentService.setCountry(aiUser.getIpdata().getCountry());
			}
			agentService.setIpaddr(aiUser.getIp());
			agentService.setAgentreplys(aiUser.getAireply());
			agentService.setUserasks(aiUser.getUserask());
			agentService.setUsername(aiUser.getUsername());
			agentService.setChannel(aiUser.getChannel());
			
			if(!StringUtils.isBlank(aiUser.getContextid())) {
				agentService.setContextid(aiUser.getContextid());
			}else {
				agentService.setContextid(aiUser.getSessionid());
			}
			
			agentService.setUserid(aiUser.getUserid());
			
			agentService.setAiid(aiUser.getAiid());
			agentService.setAiservice(true);
			agentService.setStatus(UKDataContext.AgentUserStatusEnum.INSERVICE.toString());
			
			agentService.setAppid(aiUser.getAppid());
			agentService.setLeavemsg(false);
		}
		if(agentService!=null) {
			agentService.setEndby(endby);
			if(agentService.getUserasks() > 0 || agentService.getAgentreplys() > 0) {
				agentService.setUseful(true);
			}else {
				agentService.setUseful(false);
			}
		}
		agentServiceRes.save(agentService) ;
		return agentService ;
	}
	
	/**
	 * 为访客 分配坐席， ACD策略，此处 AgentStatus 是建议 的 坐席，  如果启用了  历史服务坐席 优先策略， 则会默认检查历史坐席是否空闲，如果空闲，则分配，如果不空闲，则 分配当前建议的坐席
	 * @param agentStatus
	 * @param agentUser
	 * @param orgi
	 * @return
	 * @throws Exception
	 */
	private static AgentService processAgentService(AgentStatus agentStatus , AgentUser agentUser , String orgi , boolean finished) throws Exception{
		AgentService agentService = new AgentService();	//放入缓存的对象
		AgentServiceRepository agentServiceRepository = UKDataContext.getContext().getBean(AgentServiceRepository.class) ;
		
		if(!StringUtils.isBlank(agentUser.getAgentserviceid())) {
			AgentService tempAgentService = agentServiceRepository.findByIdAndOrgi(agentUser.getAgentserviceid(), agentUser.getOrgi()) ;
			if(tempAgentService!=null && tempAgentService.getAgentno()!=null && agentStatus!=null && tempAgentService.getAgentno().equals(agentStatus.getAgentno())) {
				agentService.setId(agentUser.getAgentserviceid());
			}else if(tempAgentService == null || StringUtils.isBlank(tempAgentService.getAgentno())){
				agentService.setId(agentUser.getAgentserviceid());
			}
		}
		agentService.setOrgi(orgi);
		
		UKTools.copyProperties(agentUser, agentService); //复制属性
		
		agentService.setChannel(agentUser.getChannel());
		
		agentService.setIpaddr(agentUser.getIpaddr());
		
		agentService.setCity(agentUser.getCity());
		agentService.setProvince(agentUser.getProvince());
		agentService.setCountry(agentUser.getCountry());
		
		agentService.setSessionid(agentUser.getSessionid());
		OnlineUserESRepository onlineUserESRes = UKDataContext.getContext().getBean(OnlineUserESRepository.class) ;
		OnlineUserRepository onlineUserRes = UKDataContext.getContext().getBean(OnlineUserRepository.class) ;
		agentUser.setLogindate(new Date());
		List<OnlineUser> onlineUserList = onlineUserESRes.findByUseridAndOrgi(agentUser.getUserid() , agentUser.getOrgi());
		OnlineUser onlineUser = null ;
		if (onlineUserList.size() > 0) {
			onlineUser = onlineUserList.get(0) ;
		}
		
		Date lastDate = new Date(System.currentTimeMillis() - 60000);
		Page<AgentService> aiServiceList = agentServiceRepository.findByUseridAndAiserviceAndOrgiAndEndtimeGreaterThan(agentUser.getUserid() , true , agentUser.getOrgi() , lastDate , new PageRequest(0, 1, Sort.Direction.ASC, "endtime"));
		if(aiServiceList!=null && aiServiceList.getContent().size() > 0) {
			AgentService aiService = aiServiceList.getContent().get(0) ;
			aiService.setTransagent(true);
			aiService.setTranstime(new Date());
			aiService.setTranstraceid(agentService.getId());
			aiService.setUseful(true);
			
			agentServiceRepository.save(aiService) ;
			
			agentService.setFromai(true);
			agentService.setAiserviceid(aiService.getId());
		}
		
		if(agentStatus!=null){
			SessionConfig sessionConfig = initSessionConfig(orgi) ;
			
			agentService.setAgent(agentStatus.getAgentno());
			agentService.setSkill(agentUser.getSkill());
			
			if(sessionConfig.isLastagent() && agentUser.isRefuse() == false){	//启用了历史坐席优先 ， 查找 历史服务坐席
				List<WebIMReport> webIMaggList = UKTools.getWebIMDataAgg(onlineUserRes.findByOrgiForDistinctAgent(orgi, agentUser.getUserid())) ;
				if(webIMaggList.size()>0){
					for(WebIMReport report : webIMaggList){
						if(report.getData().equals(agentStatus.getAgentno())){
							break ;
						}else{
							AgentStatus hisAgentStatus = (AgentStatus) CacheHelper.getAgentStatusCacheBean().getCacheObject(report.getData(), orgi) ;
							if(hisAgentStatus!=null && maxuser(hisAgentStatus, sessionConfig.getMaxuser())){
								agentStatus = hisAgentStatus ;	//变更为 历史服务坐席
								break ;
							}
						}
						
					}
				}
			}
			
			agentUser.setStatus(UKDataContext.AgentUserStatusEnum.INSERVICE.toString());
			agentService.setStatus(UKDataContext.AgentUserStatusEnum.INSERVICE.toString());
			
			agentService.setSessiontype(UKDataContext.AgentUserStatusEnum.INSERVICE.toString());

			agentService.setAgentno(agentStatus.getUserid());
			agentService.setAgentusername(agentStatus.getUsername());	//agent
		}else{
			Date current = new Date();
			if(agentUser.getQueuetime() == null) {
				agentUser.setQueuetime(current);
			}
			if(finished == true) {
				agentUser.setStatus(UKDataContext.AgentUserStatusEnum.END.toString());
				agentService.setStatus(UKDataContext.AgentUserStatusEnum.END.toString());
				agentService.setSessiontype(UKDataContext.AgentUserStatusEnum.END.toString());
				if(agentStatus==null) {
					agentService.setLeavemsg(true); //是留言
					agentService.setLeavemsgstatus(UKDataContext.LeaveMsgStatus.NOTPROCESS.toString()); //未处理的留言
				}
				agentService.setEndtime(current);
			}else {
				agentUser.setStatus(UKDataContext.AgentUserStatusEnum.INQUENE.toString());
				agentService.setStatus(UKDataContext.AgentUserStatusEnum.INQUENE.toString());
				
				agentService.setSessiontype(UKDataContext.AgentUserStatusEnum.INQUENE.toString());
				agentService.setQueuetime(agentUser.getQueuetime());
			}
		}
		if(finished || agentStatus!=null) {
			//			agentService.setId(null);
	
			agentService.setAgentuserid(agentUser.getId());
			
			agentService.setInitiator(UKDataContext.ChatInitiatorType.USER.toString());
	
			long waittingtime = 0  ;
			if(agentUser.getWaittingtimestart()!=null){
				waittingtime = System.currentTimeMillis() - agentUser.getWaittingtimestart().getTime() ;
			}else if(agentUser.getCreatetime()!=null){
				waittingtime = System.currentTimeMillis() - agentUser.getCreatetime().getTime() ;
			}
			agentUser.setWaittingtime((int)waittingtime);
	
			agentUser.setServicetime(new Date());
	
			agentService.setOwner(agentUser.getOwner());
			
			agentService.setTimes(0);
			agentUser.setAgentno(agentService.getAgentno());
	
			AgentServiceRepository agentServiceRes = UKDataContext.getContext().getBean(AgentServiceRepository.class) ;
	
			if(!StringUtils.isBlank(agentUser.getName())){
				agentService.setName(agentUser.getName());
			}
			if(!StringUtils.isBlank(agentUser.getPhone())){
				agentService.setPhone(agentUser.getPhone());
			}
			if(!StringUtils.isBlank(agentUser.getEmail())){
				agentService.setEmail(agentUser.getEmail());
			}
			if(!StringUtils.isBlank(agentUser.getResion())){
				agentService.setResion(agentUser.getResion());
			}
	
			if(!StringUtils.isBlank(agentUser.getSkill())) {
				agentService.setAgentskill(agentUser.getSkill());
			}else if(agentStatus!=null) {
				agentService.setAgentskill(agentStatus.getSkill());
			}
	
			agentService.setServicetime(new Date());
			if(agentUser.getCreatetime()!=null){
				agentService.setWaittingtime((int) (System.currentTimeMillis() - agentUser.getCreatetime().getTime()));
				agentUser.setWaittingtime(agentService.getWaittingtime());
			}
			if (onlineUser != null ) {
				agentService.setOsname(onlineUser.getOpersystem());
				agentService.setBrowser(onlineUser.getBrowser());
				agentService.setDataid(onlineUser.getId());		//记录  onlineuser 的id
			}
			agentService.setLogindate(agentUser.getCreatetime());
			agentServiceRes.save(agentService) ;
			agentUser.setLastgetmessage(new Date());
			agentUser.setLastmessage(new Date());
		}
		
		agentUser.setAgentserviceid(agentService.getId());
		agentService.setDataid(agentUser.getId());
		/**
		 * 分配成功以后， 将用户 和坐席的对应关系放入到 缓存
		 */
		/**
		 * 将 AgentUser 放入到 当前坐席的 服务队列
		 */
		AgentUserRepository agentUserRepository = UKDataContext.getContext().getBean(AgentUserRepository.class) ;
		
		/**
		 * 更新OnlineUser对象，变更为服务中，不可邀请
		 */
		
		if(onlineUser!=null){
			onlineUser.setInvitestatus(UKDataContext.OnlineUserInviteStatus.INSERV.toString());
			onlineUserESRes.save(onlineUser) ;
		}
		
		/**
		 * 
		 */
		agentUserRepository.save(agentUser);

		CacheHelper.getAgentUserCacheBean().put(agentUser.getUserid(), agentUser , UKDataContext.SYSTEM_ORGI) ;
		
		if(agentStatus != null){
			updateAgentStatus(agentStatus  , agentUser , orgi , true) ;
		}
		
		return agentService ;
	}
	
	public static AgentUser deleteAgentUser(AgentUser agentUser, String orgi ,String endby)
			throws Exception {
		if (agentUser!=null) {
			serviceFinish(agentUser, orgi , endby  , true);
		}
		return agentUser;
	}
	
	
	/**
	 * 
	 * @param agentStatus
	 * @return
	 */
	public static String getSuccessMessage(AgentService agentService ,String channel,String orgi){
		String queneTip = "<span id='agentno'>"+agentService.getAgentusername()+"</span>" ;
		if(!UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(channel)){
			queneTip = agentService.getAgentusername() ;
		}
		SessionConfig sessionConfig = initSessionConfig(orgi) ;
		String successMsg = "坐席分配成功，"+queneTip+"为您服务。"  ;
		if(!StringUtils.isBlank(sessionConfig.getSuccessmsg())){
			successMsg = sessionConfig.getSuccessmsg().replaceAll("\\{agent\\}", queneTip) ;
		}
		return successMsg ;
	}
	/**
	 * 转接提示信息
	 * @param agentStatus
	 * @return
	 */
	public static String getTransMessage(AgentService agentService ,String channel,String orgi){
		String queneTip = "<span id='agentno'>"+agentService.getAgentusername()+"</span>" ;
		if(!UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(channel)){
			queneTip = agentService.getAgentusername() ;
		}
		SessionConfig sessionConfig = initSessionConfig(orgi) ;
		String successMsg = "已为你转接给坐席"+queneTip;
		if(!StringUtils.isBlank(sessionConfig.getTransmsg())){
			successMsg = sessionConfig.getTransmsg().replaceAll("\\{agent\\}", queneTip) ;
		}
		return successMsg ;
	}
	/**
	 * 
	 * @param agentStatus
	 * @return
	 */
	public static String getServiceFinishMessage(String channel,String orgi){
		SessionConfig sessionConfig = initSessionConfig(orgi) ;
		String queneTip = "坐席已断开和您的对话" ;
		if(!StringUtils.isBlank(sessionConfig.getFinessmsg())){
			queneTip = sessionConfig.getFinessmsg();
		}
		return queneTip ;
	}
	
	public static String getNoAgentMessage(int queneIndex , String channel,String orgi){
		if(queneIndex < 0){
			queneIndex = 0 ;
		}
		String queneTip = "<span id='queneindex'>"+queneIndex+"</span>" ;
		if(!UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(channel)){
			queneTip = String.valueOf(queneIndex) ;
		}
		SessionConfig sessionConfig = initSessionConfig(orgi) ;
		String noAgentTipMsg = "坐席全忙，已进入等待队列，您也可以在其他时间再来咨询。"  ;
		if(!StringUtils.isBlank(sessionConfig.getNoagentmsg())){
			noAgentTipMsg = sessionConfig.getNoagentmsg().replaceAll("\\{num\\}", queneTip) ;
		}
		return noAgentTipMsg;
	}
	public static String getQueneMessage(int queneIndex , String channel,String orgi){
		
		String queneTip = "<span id='queneindex'>"+queneIndex+"</span>" , indexTip = "<span id='queneindex'>"+(queneIndex + 1)+"</span>";
		if(!UKDataContext.ChannelTypeEnum.WEBIM.toString().equals(channel)){
			queneTip = String.valueOf(queneIndex) ;
			indexTip = String.valueOf(queneIndex+1) ;
		}
		SessionConfig sessionConfig = initSessionConfig(orgi) ;
		String agentBusyTipMsg = "正在排队，请稍候,在您之前，还有  <span id='queueindex'>"+queneTip+"</span> 位等待用户。"  ;
		if(!StringUtils.isBlank(sessionConfig.getAgentbusymsg())){
			agentBusyTipMsg = sessionConfig.getAgentbusymsg().replaceAll("\\{num\\}", "<span id='queueindex'>"+queneTip+"</span>").replaceAll("\\{index\\}", "<span id='queueindex'>"+indexTip+"</span>") ;
		}
		return agentBusyTipMsg;
	}
	
	/**
	 * 坐席离线 
	 * @param userid
	 * @param status
	 */
	public static void deleteAgentStatus(String userid , String orgi , boolean isAdmin) {
		AgentStatusRepository agentStatusRes = UKDataContext.getContext().getBean(AgentStatusRepository.class) ;
		List<AgentStatus> agentStatusList = agentStatusRes.findByAgentnoAndOrgi(userid , orgi);
		for(AgentStatus agentStatus : agentStatusList){
			ServiceQuene.recordAgentStatus(agentStatus.getAgentno(),agentStatus.getUsername() , agentStatus.getAgentno(), agentStatus.getSkill(),isAdmin, agentStatus.getAgentno(), agentStatus.isBusy() ? UKDataContext.AgentStatusEnum.BUSY.toString():UKDataContext.AgentStatusEnum.NOTREADY.toString(), UKDataContext.AgentStatusEnum.NOTREADY.toString(), UKDataContext.AgentWorkType.MEIDIACHAT.toString() , agentStatus.getOrgi() , agentStatus.getUpdatetime());
			agentStatusRes.delete(agentStatus);
		}
    	CacheHelper.getAgentStatusCacheBean().delete(userid,orgi);
    	ServiceQuene.publishMessage(orgi , "agent" , "leave" , userid);
	}
}
