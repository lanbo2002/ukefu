package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.DataProduct;

public abstract interface DataProductRepository extends JpaRepository<DataProduct, String> {
	
	public abstract DataProduct findByIdAndOrgi(String id, String orgi);

//	public abstract Page<DataProduct> findByOrgi(String orgi , Pageable page) ;
	
	public abstract Page<DataProduct> findByOrgiAndScheme(String orgi ,String scheme , Pageable page) ;
	
//	public abstract Page<DataProduct> findByOrgiAndParentid(String orgi ,String parentid , Pageable page) ;
	
	public abstract Page<DataProduct> findByOrgiAndParentidAndScheme(String orgi ,String parentid ,String scheme, Pageable page) ;
	
	public abstract List<DataProduct> findByOrgiAndParentid(String orgi ,String parentid) ;
	
	public abstract List<DataProduct> findByOrgiAndMenuid(String orgi ,String menuid) ;
	
	public abstract List<DataProduct> findByOrgiAndMenuidOrderBySortindexAsc(String orgi ,String menuid) ;
	
//	public abstract List<DataProduct> findByOrgi(String orgi) ;
	
	public abstract List<DataProduct> findByOrgiAndScheme(String orgi,String scheme) ;
	
//	public abstract Page<DataProduct> findByOrgiAndNameLike(String orgi ,String name , Pageable page) ;
	
	public abstract Page<DataProduct> findByOrgiAndSchemeAndNameLike(String orgi ,String scheme,String name , Pageable page) ;
	
//	public abstract Page<DataProduct> findByOrgiAndParentidAndNameLike(String orgi ,String parentid,String name , Pageable page) ;
	
	public abstract Page<DataProduct> findByOrgiAndParentidAndSchemeAndNameLike(String orgi ,String parentid,String scheme,String name , Pageable page) ;
	
	public abstract int countByOrgiAndNameLike(String orgi ,String name) ;
	
//	public abstract int countByOrgiAndParentidAndNameLike(String orgi ,String parentid,String name) ;
	
	public abstract int countByOrgiAndParentidAndSchemeAndNameLike(String orgi ,String parentid,String scheme,String name) ;
	
	public int countByOrgiAndName(String orgi , String name);
	
//	public abstract List<DataProduct> findByTypeAndOrgi(String type ,String orgi) ;
	
	public abstract List<DataProduct> findByTypeAndOrgiAndScheme(String type ,String orgi,String scheme) ;
	
	public abstract List<DataProduct> findByOrgiAndMenuidAndParentid(String orgi ,String menuid,String parentid) ;
	
	public abstract DataProduct findByCodeAndSchemeAndOrgi(String code,String scheme, String orgi);
	
	public abstract List<DataProduct> findByOrgiAndParentidAndDefaulshow(String orgi ,String parentid,boolean defaulshow) ;
	
	public abstract List<DataProduct> findByOrgiAndMenuidAndDefaulshow(String orgi ,String menuid,boolean defaulshow) ;
	
	public abstract DataProduct findByTbid(String tbid);
	
	public abstract DataProduct findByDatamodelid(String datamodelid);
}
