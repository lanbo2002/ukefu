package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.StatusEventAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusEventAnswerRepository extends JpaRepository<StatusEventAnswer, String> {

}
