package com.ukefu.webim.service.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.AgentUserTask;

public abstract interface AgentUserTaskRepository  extends JpaRepository<AgentUserTask, String>{
	
	public List<AgentUserTask> findByIdAndOrgi(String id , String orgi);
	
	public List<AgentUserTask> findByLastmessageLessThanAndStatusAndOrgi(Date start , String status , String orgi) ;
	
	public List<AgentUserTask> findByLastgetmessageLessThanAndStatusAndOrgi(Date start , String status , String orgi) ;
	
	public List<AgentUserTask> findByTransfertimeLessThanAndStatusAndOrgi(Date start , String status , String orgi) ;
	
	public List<AgentUserTask> findByLogindateLessThanAndStatusAndOrgi(Date start , String status , String orgi) ;
	
	public List<AgentUserTask> findByEndtimeLessThanAndStatusAndOrgi(Date endtime , String status , String orgi) ;
	
	public List<AgentUserTask> findByLogindateLessThanAndOrgi(Date start , String orgi) ;
	
	public Page<AgentUserTask> findAll(Specification<AgentUserTask> spec, Pageable pageable);  //分页按条件查询
	
	public AgentUserTask findByUseridAndOrgi(String userid , String orgi);
	
	public List<AgentUserTask> findByServicetimeLessThanAndStatusAndOrgi(Date start ,String status, String orgi);
}

