package com.ukefu.webim.service.es;

import com.ukefu.webim.web.model.QuickReply;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface QuickReplyRepository extends  ElasticsearchRepository<QuickReply, String> , QuickReplyEsCommonRepository {
    public QuickReply findByIdAndOrgi(String id,String orgi) ;

    public List<QuickReply> findByIdInAndOrgi(List<String> id, String orgi) ;

}
