package com.ukefu.webim.service.es;

import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Page;

import com.ukefu.webim.web.model.Topic;
import org.springframework.data.domain.Pageable;

public interface TopicEsCommonRepository {
	public Page<Topic> getTopicByCateAndOrgi(String cate ,String orgi,String q, int p, int ps) ;
	
	public Page<Topic> getTopicByAiidAndOrgi(String aiid ,String orgi,String q, int p, int ps) ;
	
	public Page<Topic> getTopicByTopAndOrgi(boolean top ,String orgi , String aiid, int p, int ps) ;
	
	public List<Topic> getTopicByOrgi(String orgi, String type , String q,Pageable page) ;
	
	public Page<Topic> getTopicByCateAndUserAndOrgi(String cate , String q ,String user,String Orgi , int p, int ps) ;
	
	public Page<Topic> getTopicByCon(BoolQueryBuilder booleanQueryBuilder , int p, int ps) ;
	
	public Topic findByIdAndOrgi(String id, String orgi ) ;
	
	public List<Topic> findByIdInAndOrgi(List<String> ids, String orgi ) ;
	
	public Page<Topic> findByOrgi(String orgi,Pageable page) ;
}
