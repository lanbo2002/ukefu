package com.ukefu.webim.service.es;

import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import com.ukefu.webim.web.model.VoiceTranscription;

public class VoiceTranscriptionRepositoryImpl implements VoiceTranscriptionESRepository{
	
private ElasticsearchTemplate elasticsearchTemplate;
	
	@Autowired
	public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
		this.elasticsearchTemplate = elasticsearchTemplate;
    }
	
	private Page<VoiceTranscription> processQuery(BoolQueryBuilder boolQueryBuilder, Pageable page){
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder).withSort(new FieldSortBuilder("createtime").unmappedType("date").order(SortOrder.DESC));
		
		searchQueryBuilder.withPageable(page) ;
		
		Page<VoiceTranscription> entCustomerList = null ;
		if(elasticsearchTemplate.indexExists(VoiceTranscription.class)){
			entCustomerList = elasticsearchTemplate.queryForPage(searchQueryBuilder.build() , VoiceTranscription.class) ;
		}
		return entCustomerList;
	}
	
	public VoiceTranscription proccessQuery(BoolQueryBuilder boolQueryBuilder){
		NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder) ;
		List<VoiceTranscription> voiceTranscriptionList = null ;
		VoiceTranscription voiceTranscription = null;
		if(elasticsearchTemplate.indexExists(VoiceTranscription.class)){
			voiceTranscriptionList = elasticsearchTemplate.queryForList(searchQueryBuilder.build(), VoiceTranscription.class );
		}
		if(voiceTranscriptionList != null && voiceTranscriptionList.size() > 0) {
			voiceTranscription = voiceTranscriptionList.get(0);
		}
		return voiceTranscription;
	}
	
	@Override
	public VoiceTranscription findByIdAndOrgi(String id, String orgi) {
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.termQuery("id", id)) ;
		boolQueryBuilder.must(QueryBuilders.termQuery("orgi", orgi)) ;
		return proccessQuery(boolQueryBuilder);
	}

	@Override
	public List<VoiceTranscription> findByCallidAndOrgi(String callid, String orgi) {
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.termQuery("callid", callid)) ;
		boolQueryBuilder.must(QueryBuilders.termQuery("orgi", orgi)) ;
		return processQuery(boolQueryBuilder, new PageRequest(0, 10000)).getContent();
	}

	@Override
	public Page<VoiceTranscription> findByOrgi(BoolQueryBuilder boolQueryBuilder, Pageable page) {
		return processQuery(boolQueryBuilder, page);
	}

}
